/*
 * Copyright 2002-2014 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example;

import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

//com.example.Main
public class Main {

	private static final int DEFAULT_PORT = -1;//8092; -1 to make the change mandatory
	public static final boolean BROADCAST = true;
	private List<ClientTask> clientServices = new ArrayList<ClientTask>();

	public void broadcast(ClientTask serviceTask, String data) {
		for (ClientTask client : clientServices) 
			if (client != serviceTask)
				client.send(data);
		
	}

	public void remove(ClientTask clientTask) {
		clientServices.remove(clientTask);	
	}

	public void runServer() throws Exception {
		

        String webPort = System.getenv("PORT");
        if (webPort == null || webPort.isEmpty()) {
            webPort = Integer.toString(DEFAULT_PORT);
        //    webPort =  getPortFromProperties();
        } 


        int wport = Integer.valueOf(webPort);
		
		ServerSocket socketEcoute = new ServerSocket(wport);
		Util.log(null, "[serveur multisession] démarré sur :" +  ":" + socketEcoute.getLocalPort());

		//boolean endServer = false;
		int numClient = 0;
		while (!Util.globalEnd) {
			Util.log(null, "en attente d'une connexion");
			Socket socketService = socketEcoute.accept(); // bloquant ici
			ClientTask clientTask = new ClientTask(this,numClient++);
			clientServices.add(clientTask);
			clientTask.setSocketService(socketService);
			clientTask.start();
		}
		socketEcoute.close();
		Util.log(null, "arrêt du serveur");
	}

	public static void main(String[] args) {
		Main threadedServer = new Main();
		try {
			threadedServer.runServer();
		} catch (Exception e) {
			Util.log(e.toString());
		}
	}

}
//test on heroku ko
//test on vpn ok